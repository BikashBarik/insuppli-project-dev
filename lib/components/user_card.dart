import 'package:flutter/material.dart';
import 'package:mongodb_flutter/models/user.dart';

class UserCard extends StatelessWidget {
  UserCard({this.user, this.onTapDelete, this.onTapEdit});
  final User user;
  final Function onTapEdit, onTapDelete;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 2.0,
      color: Colors.white,
      child: ListTile(
        leading: CircleAvatar(
          backgroundImage: AssetImage('assets/pr.png'),
          backgroundColor:
              Colors.transparent, // Set the background color to transparent
          foregroundColor:
              Colors.black, // Set the foreground (border) color to black
          radius: 25, // Adjust the radius value as per your requirement
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: Colors.black,
                width: 3,
              ),
            ),
            child: ClipOval(
              child: Image.asset('assets/pr.png'),
            ),
          ),
        ),
        title: Text("Product Name:  ${user.productname}"),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Price : ${user.price}'),
            Text('Part No. :${user.partno}'),
          ],
        ),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              child: Icon(Icons.edit),
              onTap: onTapEdit,
            ),
            GestureDetector(
              child: Icon(Icons.delete),
              onTap: onTapDelete,
            ),
          ],
        ),
      ),
    );
  }
}
