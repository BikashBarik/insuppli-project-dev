import 'package:mongo_dart/mongo_dart.dart';
import 'package:fixnum/fixnum.dart';

class User {
  final ObjectId id;
  final String productname;
  final int partno;
  final int price;

  const User({this.id, this.productname, this.partno, this.price});

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'productname': productname,
      'partno': partno,
      'price': price,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['_id'],
      productname: map['productname'],
      price: (map['price'] is Int64) ? map['price'].toInt() : map['price'],
      partno: (map['partno'] is Int64) ? map['partno'].toInt() : map['partno'],
    );
  }
}
