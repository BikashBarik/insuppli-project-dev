import 'package:mongo_dart/mongo_dart.dart';
import 'package:fixnum/fixnum.dart';

class AddUser {
  final ObjectId id;
  final String fname;
  final String lname;
  final String cname;
  final String email;
  final String pass;

  const AddUser(
      {this.id, this.fname, this.lname, this.cname, this.email, this.pass});

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'fname': fname,
      'lname': lname,
      'cname': cname,
      'email': email,
      'pass': pass,
    };
  }

  factory AddUser.fromMap(Map<String, dynamic> map) {
    return AddUser(
      id: map['_id'],
      fname: map['fname'],
      lname: map['lname'],
      cname: map['cname'],
      email: map['email'],
      pass: map['pass'],
    );
  }
}
