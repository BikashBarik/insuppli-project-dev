import 'package:flutter/material.dart';
import 'package:mongodb_flutter/pages/add_user_page.dart';
import 'package:mongodb_flutter/pages/edit_profile_page.dart';
import 'package:mongodb_flutter/pages/contact_support.dart';
import 'package:mongodb_flutter/pages/delete_account.dart';
import 'package:mongodb_flutter/pages/terms.dart';
import 'package:mongodb_flutter/database/database.dart';
import 'package:mongodb_flutter/models/user.dart';
import 'package:mongodb_flutter/components/user_card.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int age;
  bool _showRecentSearches = false;
  List<String> recentSearches = [];
  TextEditingController _searchController;

  void setAgeFilter(int enteredAge) {
    setState(() {
      age = enteredAge;
    });
  }

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void onRecentSearchTap(String query) {
    setState(() {
      _searchController.text = query;
    });
  }

  Widget buildRecentSearchesSection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 16.0),
        if (recentSearches.isNotEmpty)
          Text(
            'Recent Searches:',
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
        if (recentSearches.isEmpty)
          Text(
            'Welcome to the Home Page!',
            style: TextStyle(fontSize: 24.0),
          ),
        SizedBox(height: 16.0),
        Expanded(
          child: ListView.separated(
            itemCount: recentSearches.length,
            separatorBuilder: (BuildContext context, int index) {
              return Divider(
                color: Colors.black, // Set the line color to black
                thickness: 1.0, // Set the line thickness
              );
            },
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: Row(
                  children: [
                    Expanded(
                      child: Text(recentSearches[index]),
                    ),
                    IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        setState(() {
                          recentSearches.removeAt(index);
                        });
                      },
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  void searchQuery(String query) {
    // Implement your search query logic here
    print('Searching for: $query');
    // Example: Add the search query to the recent searches list
    setState(() {
      recentSearches.insert(0, query);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 14, 13, 13),
        title: Text(
          'Search',
          style: TextStyle(
            fontSize: 28,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
              size: 30,
            ),
            onPressed: () {
              int enteredAge = int.tryParse(_searchController.text);
              if (enteredAge != null) {
                setAgeFilter(enteredAge);
              }
            },
          ),
          PopupMenuButton<String>(
            onSelected: (value) {
              if (value == 'edit_profile') {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EditProfilePage(),
                  ),
                );
              } else if (value == 'support') {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ContactFormPage(),
                  ),
                );
              } else if (value == 'terms') {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Termspage(),
                  ),
                );
              } else if (value == 'delete_account') {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => DeleteAccountPopup(),
                );
              } else if (value == 'sign_out') {
                // Handle "Sign Out" selection
              }
            },
            itemBuilder: (BuildContext context) => [
              PopupMenuItem<String>(
                value: 'edit_profile',
                child: Text('Edit Profile'),
              ),
              PopupMenuItem<String>(
                value: 'support',
                child: Text('Support'),
              ),
              PopupMenuItem<String>(
                value: 'terms',
                child: Text('Terms'),
              ),
              PopupMenuItem<String>(
                value: 'delete_account',
                child: Text('Delete Account'),
              ),
              PopupMenuItem<String>(
                value: 'sign_out',
                child: Text('Sign Out'),
              ),
            ],
          ),
        ],
      ),
      backgroundColor: Colors.black,
      body: Column(
        children: [
          Image.asset(
            'assets/insupplii.png',
            width: 200,
            height: 100,
          ),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Text.rich(
              TextSpan(
                text: 'Search, Find, Buy\n',
                style: TextStyle(fontSize: 18, color: Colors.white),
                children: [
                  TextSpan(
                    text: 'Industrial Supplies Near You',
                  ),
                ],
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: TextField(
              controller: _searchController,
              onChanged: searchQuery,
              onTap: () {
                setState(() {
                  _showRecentSearches = true; // Show recent searches
                });
              },
              decoration: InputDecoration(
                hintText: 'Enter your search query',
                hintStyle: TextStyle(color: Colors.grey),
                filled: true,
                fillColor: Colors.white,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _showRecentSearches = true; // Show recent searches
                });
              },
              child: TextField(
                controller: _searchController,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  hintText: 'Search...',
                  hintStyle: TextStyle(color: Colors.grey),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ),
          ),
          if (recentSearches.isNotEmpty) buildRecentSearchesSection(),
          Expanded(
            child: FutureBuilder(
              future: age != null
                  ? MongoDatabase.getDocumentsByAge(age)
                  : MongoDatabase.getDocuments(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  if (snapshot.hasError) {
                    return Center(
                      child: Text(
                        'Something went wrong, try again.',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    );
                  } else {
                    return ListView.builder(
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: UserCard(
                            user: User.fromMap(snapshot.data[index]),
                            onTapDelete: () async {
                              _deleteUser(User.fromMap(snapshot.data[index]));
                            },
                            onTapEdit: () async {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) {
                                    return AddUserPage();
                                  },
                                  settings: RouteSettings(
                                    arguments:
                                        User.fromMap(snapshot.data[index]),
                                  ),
                                ),
                              ).then((value) => setState(() {}));
                            },
                          ),
                        );
                      },
                      itemCount: snapshot.data.length,
                    );
                  }
                }
              },
            ),
          ),
          SizedBox(height: 16.0),
          if (_showRecentSearches)
            Text(
              'Recent Searches:',
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
          if (!_showRecentSearches)
            Text(
              'Welcome to the Home Page!',
              style: TextStyle(fontSize: 24.0),
            ),
          SizedBox(height: 16.0),
          Expanded(
            child: ListView.separated(
              itemCount: recentSearches.length,
              separatorBuilder: (BuildContext context, int index) {
                return Divider(
                  color: Colors.black, // Set the line color to black
                  thickness: 1.0, // Set the line thickness
                );
              },
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Row(
                    children: [
                      Expanded(
                        child: Text(recentSearches[index]),
                      ),
                      IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () {
                          setState(() {
                            recentSearches.removeAt(index);
                          });
                        },
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) {
                return AddUserPage();
              },
            ),
          ).then((value) => setState(() {}));
        },
        child: Icon(Icons.add),
      ),
    );
  }

  void _deleteUser(User user) async {
    await MongoDatabase.delete(
        user); // Pass the entire user object to the delete method
    setState(() {});
  }
}
