import 'package:flutter/material.dart';
import 'package:mongo_dart/mongo_dart.dart' as M;
import 'package:mongodb_flutter/database/database.dart';
import 'package:mongodb_flutter/models/adduser.dart';
import 'package:flutter/gestures.dart';
import 'package:mongodb_flutter/pages/home_page.dart';
import 'package:mongodb_flutter/pages/login.dart';

class RegistrationPage extends StatelessWidget {
  const RegistrationPage({Key key}) : super(key: key);

  static const String _title = 'Registration Page';

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: _title,
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         backgroundColor: Colors.black,
//         body: Column(
//           children: [
//             Expanded(
//               child: MyStatefulWidget(),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          children: [
            Container(
              padding: const EdgeInsets.fromLTRB(10, 60, 10, 0),
              color: Color.fromARGB(255, 36, 36, 36),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(Icons.close, color: Colors.green),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LoginPage(),
                        ),
                      );
                    },
                  ),
                  Text(
                    'Sign Up',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(width: 40), // Add spacing for center alignment
                ],
              ),
            ),
            Expanded(
              child: MyStatefulWidget(),
            ),
          ],
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController companyNameController = TextEditingController();
  TextEditingController emailAddressController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    firstNameController.dispose();
    lastNameController.dispose();
    companyNameController.dispose();
    emailAddressController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final AddUser user = ModalRoute.of(context).settings.arguments;
    if (user != null) {
      firstNameController.text = user.fname;
      lastNameController.text = user.lname;
      companyNameController.text = user.cname;
      emailAddressController.text = user.email;
      passwordController.text = user.pass;
    }
    return Padding(
      padding: const EdgeInsets.all(10),
      child: ListView(
        children: <Widget>[
          Transform.translate(
            offset: const Offset(0, -30),
            child: Container(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child:
                  Image(image: AssetImage('assets/insupplii.png'), height: 80),
            ),
          ),

          // Container(
          // padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          // child: Image(image: AssetImage('assets/insupplii.png'), height: 80),
          // ),
          Transform.translate(
            offset: const Offset(0, 10),
            child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 30),
              child: const Text(
                'Enter Account Details Below',
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(left: 20),
            child: TextField(
              controller: firstNameController,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'First Name',
                hintStyle: TextStyle(
                  color: Colors.white70,
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(left: 20),
            child: TextField(
              controller: lastNameController,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Last Name',
                hintStyle: TextStyle(
                  color: Colors.white70,
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(left: 20),
            child: TextField(
              controller: companyNameController,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Company Name',
                hintStyle: TextStyle(
                  color: Colors.white70,
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(left: 20),
            child: TextField(
              controller: emailAddressController,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Email Address',
                hintStyle: TextStyle(
                  color: Colors.white70,
                ),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            margin: const EdgeInsets.only(left: 20),
            child: TextField(
              obscureText: true,
              controller: passwordController,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Password',
                hintStyle: TextStyle(
                  color: Colors.white70,
                ),
              ),
            ),
          ),
          Container(
            height: 40,
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            margin: const EdgeInsets.only(left: 15, right: 15, top: 20),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(40),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(255, 124, 241, 127),
                ),
                child: const Text(
                  'Create Account',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  insertUser();
                },
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: RichText(
              text: TextSpan(
                text:
                    'iNSUPPLi helps you find the best prices. By clicking the Create Account button, I agree to the ',
                style: TextStyle(color: Colors.white),
                children: [
                  TextSpan(
                    text: 'Terms of Service',
                    style: TextStyle(
                      color: Colors.white,
                      decoration: TextDecoration.underline,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        // Handle Terms of Service tapped
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => RegistrationPage(),
                          ),
                        );
                      },
                  ),
                  TextSpan(
                    text: '.',
                  ),
                ],
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  insertUser() async {
    final adduser = AddUser(
      id: M.ObjectId(),
      fname: firstNameController.text,
      lname: lastNameController.text,
      cname: companyNameController.text,
      email: emailAddressController.text,
      pass: passwordController.text,
    );
    await MongoDatabase.insertAddUser(adduser);
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => HomePage()),
    );
  }
}
