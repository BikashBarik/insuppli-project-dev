// ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors

import 'package:flutter/material.dart';

class Termspage extends StatefulWidget {
  @override
  _TermspageState createState() => _TermspageState();
}

class _TermspageState extends State<Termspage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(kToolbarHeight + 20),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.red,
            border: Border(
              bottom: BorderSide(
                color: Colors.black, // Customize the border color here
                width: 8.0, // Customize the border width here
              ),
            ),
          ),
          child: AppBar(
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.black), // Change the back button color here
            title: Row(
              children: [
                Image.asset(
                  'assets/mpower_logo.png', // Replace with the path to your image
                  width: 200, // Adjust the width to fit your design
                ),
                const SizedBox(width: 8),
                Text(''),
              ],
            ),
            actions: <Widget>[
              PopupMenuButton(
                color: Color.alphaBlend(Colors.black, Colors.white),
                itemBuilder: (BuildContext context) => [
                  // const PopupMenuItem(
                  //   child: Text('Menu Item 1'),
                  // ),
                  // const PopupMenuItem(
                  //   child: Text('Menu Item 2'),
                  // ),
                  // const PopupMenuItem(
                  //   child: Text('Menu Item 3'),
                  // ),
                ],
              ),
            ],
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text(
                'TERMS OF USE - MANUFACTURING POWER - INSUPPLI',
                style: TextStyle(fontSize: 38, color: Color.fromARGB(255, 136, 146, 153), fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: Image.asset(
                'assets/terms.png', // Replace with the path to your image
                width: 400, // Adjust the width to fit your design
              ),
            ),
          ],
        ),
      ),
    );
  }
}
