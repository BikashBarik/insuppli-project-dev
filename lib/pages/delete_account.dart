import 'package:flutter/material.dart';

class DeleteAccountPopup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor:
          Color.fromARGB(255, 77, 73, 73), // Set background color to grey
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Delete Account?',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.white, // Set text color to white
              ),
            ),
            SizedBox(height: 16),
            Text(
              'Are you sure you want to permanently delete your account? All of your redemption history will be destroyed.',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white, // Set text color to white
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  onPressed: () {
                    // Close the delete account popup
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                      color: Color.fromARGB(
                          255, 96, 62, 218), // Set button text color to white
                    ),
                  ),
                ),
                VerticalDivider(
                  color: Colors.white, // Set line color to white
                  thickness: 1, // Set line thickness
                  width: 32, // Set line width
                ),
                TextButton(
                  onPressed: () {
                    // Implement the account deletion logic here

                    // Close the delete account popup
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'Delete',
                    style: TextStyle(
                      color: Color.fromARGB(
                          255, 236, 92, 92), // Set button text color to white
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
