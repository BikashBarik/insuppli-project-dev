// import 'package:flutter/material.dart';
// import 'package:search_page_app/terms.dart';
// import 'contact_support.dart';
// import 'edit_profile_page.dart';
// import 'delete_account.dart';

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       title: 'Flutter Menu Bar',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: HomePage(),
//     );
//   }
// }

// class HomePage extends StatefulWidget {
//   @override
//   _HomePageState createState() => _HomePageState();
// }

// class _HomePageState extends State<HomePage> {
//   TextEditingController _searchController = TextEditingController();
//   bool _showCancel = false;
//   bool _showRecentSearches = false; // Track the state of recent searches visibility
//   List<String> recentSearches = [];

//   @override
//   void initState() {
//     super.initState();
//     _searchController.addListener(() {
//       setState(() {
//         _showCancel = _searchController.text.isNotEmpty;
//       });
//     });
//   }

//   @override
//   void dispose() {
//     _searchController.dispose();
//     super.dispose();
//   }

//   void _clearSearch() {
//     setState(() {
//       _searchController.clear();
//       _showCancel = false;
//       _showRecentSearches = false; // Hide recent searches when search is cleared
//     });
//   }

//   void _performSearch() {
//     String searchText = _searchController.text;
//     if (searchText.isNotEmpty) {
//       setState(() {
//         recentSearches.add(searchText);
//         _searchController.text = searchText; // Set the search text as the clicked value
//         _showRecentSearches = true; // Show recent searches when search is performed
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Color.fromARGB(255, 26, 25, 25),
//         flexibleSpace: Align(
//           alignment: Alignment.center,
//           child: Text(
//             'Search',
//             style: TextStyle(
//               fontSize: 20,
//               fontWeight: FontWeight.bold,
//               color: Colors.white,
//             ),
//           ),
//         ),
//         leading: PopupMenuButton<String>(
//           icon: Icon(Icons.menu),
//           onSelected: (value) {
//             if (value == 'edit_profile') {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                   builder: (context) => EditProfilePage(),
//                 ),
//               );
//             } else if (value == 'support') {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                   builder: (context) => ContactFormPage(),
//                 ),
//               );
//             } else if (value == 'terms') {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                   builder: (context) => Termspage(),
//                 ),
//               );
//             } else if (value == 'delete_account') {
//               showDialog(
//                 context: context,
                
//                 builder: (BuildContext context) => DeleteAccountPopup(),
//               );
//             } else if (value == 'sign_out') {
//               // Handle "Sign Out" selection
//             }
//           },
//           itemBuilder: (BuildContext context) => [
//             PopupMenuItem<String>(
//               value: 'edit_profile',
//               child: Text('Edit Profile'),
//             ),
//             PopupMenuItem<String>(
//               value: 'support',
//               child: Text('Support'),
//             ),
//             PopupMenuItem<String>(
//               value: 'terms',
//               child: Text('Terms'),
//             ),
//             PopupMenuItem<String>(
//               value: 'delete_account',
//               child: Text('Delete Account'),
//             ),
//             PopupMenuItem<String>(
//               value: 'sign_out',
//               child: Text('Sign Out'),
//             ),
//           ],
//         ),
//       ),
//       body: Padding(
//         padding: const EdgeInsets.all(16.0),
//         child: Column(
//           children: [
//             Row(
//               children: [
//                 Expanded(
//                   child: GestureDetector(
//                     onTap: () {
//                       setState(() {
//                         _showRecentSearches = true; // Show recent searches
//                       });
//                     },
//                     child: TextField(
//                       controller: _searchController,
//                       decoration: InputDecoration(
//                         filled: true,
//                         fillColor: Colors.white,
//                         hintText: 'Search...',
//                         hintStyle: TextStyle(color: Colors.grey),
//                         border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(10),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//                 if (_showCancel)
//                   GestureDetector(
//                     onTap: _clearSearch,
//                     child: Container(
//                       padding: EdgeInsets.symmetric(horizontal: 10),
//                       child: Text(
//                         'Cancel',
//                         style: TextStyle(
//                           color: Colors.blue,
//                           fontWeight: FontWeight.bold,
//                         ),
//                       ),
//                     ),
//                   ),
//                 IconButton(
//                   icon: Icon(Icons.search),
//                   onPressed: () {
//                     _performSearch();
//                   },
//                 ),
//               ],
//             ),
//             SizedBox(height: 16.0),
//             if (_showRecentSearches)
//               Text(
//                 'Recent Searches:',
//                 style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
//               ),
//             if (!_showRecentSearches)
//               Text(
//                 'Welcome to the Home Page!',
//                 style: TextStyle(fontSize: 24.0),
//               ),
//             SizedBox(height: 16.0),
//             Expanded(
//               child: ListView.separated(
//                 itemCount: recentSearches.length,
//                 separatorBuilder: (BuildContext context, int index) {
//                   return Divider(
//                     color: Colors.black, // Set the line color to black
//                     thickness: 1.0, // Set the line thickness
//                   );
//                 },
//                 itemBuilder: (BuildContext context, int index) {
//                   return ListTile(
//                     title: Row(
//                       children: [
//                         Expanded(
//                           child: Text(recentSearches[index]),
//                         ),
//                         IconButton(
//                           icon: Icon(Icons.clear),
//                           onPressed: () {
//                             setState(() {
//                               recentSearches.removeAt(index);
//                             });
//                           },
//                         ),
//                       ],
//                     ),
//                   );
//                 },
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }