// import 'package:flutter/material.dart';
// import 'package:webview_flutter_windows/webview_flutter_windows.dart';

// class WebViewPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text("Flutter WebView"),
//       ),
//       body: WebViewWindows(
//         initialUrl: 'https://manufacturingpower.com/terms-of-use',
//         javascriptMode: JavascriptMode.unrestricted,
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebViewPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: 'https://manufacturingpower.com/terms-of-use',
      // https://insuppli.com/contact-us
      withJavascript: true,
      withLocalStorage: true,
    );
  }
}



// import 'package:flutter/material.dart';
// import 'package:flutter_inappwebview/flutter_inappwebview.dart';

// class WebViewPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text("Flutter WebView"),
//       ),
//       body: InAppWebView(
//         initialUrlRequest: URLRequest(
//           url: Uri.parse('https://manufacturingpower.com/terms-of-use'),
//         ),
//         initialOptions: InAppWebViewGroupOptions(
//           crossPlatform: InAppWebViewOptions(
//             javaScriptEnabled: true,
//           ),
//         ),
//         onLoadError: (controller, url, code, message) {
//           // Handle web view load errors here
//           print('WebView Error: $code, $message');
//         },
//       ),
//     );
//   }
// }
