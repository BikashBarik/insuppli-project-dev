import 'package:flutter/material.dart';
import 'package:mongodb_flutter/models/adduser.dart';
import 'package:mongo_dart/mongo_dart.dart' as M;
import 'package:mongodb_flutter/database/database.dart';
import 'package:mongodb_flutter/models/user.dart';
import 'package:mongodb_flutter/pages/home_page.dart';
import 'package:mongodb_flutter/pages/signup.dart';
import 'package:mongodb_flutter/pages/forget_password.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
        ),
        body: LoginStatefulWidget(),
        backgroundColor: Colors.black,
      ),
    );
  }
}

class LoginStatefulWidget extends StatefulWidget {
  const LoginStatefulWidget({Key key}) : super(key: key);

  @override
  State<LoginStatefulWidget> createState() => _LoginStatefulWidgetState();
}

class _LoginStatefulWidgetState extends State<LoginStatefulWidget> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  Future<void> login() async {
    String email = emailController.text.trim();
    String password = passwordController.text.trim();

    try {
      final user = await MongoDatabase.login(email, password);

      if (user != null) {
        // Login successful, navigate to the home page
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
        );
      } else {
        // Login failed, show an error message or perform any other required actions
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Login Failed'),
              content: const Text('Invalid email or password.'),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('OK'),
                ),
              ],
            );
          },
        );
      }
    } catch (e) {
      // Handle any exceptions that occur during the login process
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Login Failed'),
            content: Text('Failed to perform login: $e'),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text('OK'),
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: ListView(
        children: <Widget>[
          Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.all(10),
            child: Image.asset(
              'assets/insupplii.png',
              width: 300,
              height: 80,
            ),
          ),
          // Container(
//             padding: const EdgeInsets.all(10),
          // margin: const EdgeInsets.only(left: 20, top: 1),
          // child: Center(
          // child: Text(
          // 'Login to insuppli',
          // style: TextStyle(
          // color: Colors.white,
          // fontSize: 20,
          // fontWeight: FontWeight.bold,
          // ),
          // ),
          // ),
          // ),
          Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(left: 20, top: 50),
            child: TextField(
              controller: emailController,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                // border: OutlineInputBorder(),
                border: InputBorder.none,

                hintText: 'Email Address',
                hintStyle: TextStyle(color: Colors.white70),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(left: 20),
            child: TextField(
              obscureText: true,
              controller: passwordController,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                // border: OutlineInputBorder(),
                border: InputBorder.none,
                hintText: 'Password',
                hintStyle: TextStyle(color: Colors.white70),
              ),
            ),
          ),
          Container(
            height: 40,
            width: 10,
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            margin: const EdgeInsets.only(left: 15, right: 15, top: 20),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(40),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(255, 124, 241, 127),
                ),
                child: const Text(
                  'Login',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold, // Set the text color as black
                  ),
                ),
                onPressed: login,
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ForgetPasswordPage(),
                ),
              );
              // Forgot password screen
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 20),
              alignment: Alignment.center,
              child: Text(
                'Forgot Password?',
                style: TextStyle(
                  color: Colors.white,
                  fontStyle: FontStyle.italic,
                  // decoration: TextDecoration.underline,
                ),
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              children: <Widget>[
                const Text(
                  "Don't have an account? ",
                  style: TextStyle(
                    color: Color.fromARGB(255, 124, 241, 127),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => RegistrationPage(),
                      ),
                    );
                  },
                  child: Text(
                    'Sign Up',
                    style: TextStyle(
                      fontSize: 15,
                      color: Color.fromARGB(255, 124, 241, 127),
                    ),
                  ),
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
          )

          // Row(
//             children: <Widget>[
          // const Text(
          // "Don't have an account? ",
          // style: TextStyle(
          // color: Color.fromARGB(255, 4, 131, 8),
          // ),
          // ),
          // GestureDetector(
          // onTap: () {
          // Navigator.push(
          // context,
          // MaterialPageRoute(
          // builder: (context) => RegistrationPage(),
          // ),
          // );
          // },
          // child: Text(
          // 'Sign Up',
          // style: TextStyle(
          // fontSize: 15,
          // color: Color.fromARGB(255, 4, 131, 8),
          // ),
          // ),
          // ),
          // ],
          // mainAxisAlignment: MainAxisAlignment.center,
          // ),
        ],
      ),
    );
  }
}
