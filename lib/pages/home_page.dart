import 'package:flutter/material.dart';
import 'package:mongodb_flutter/pages/add_user_page.dart';
import 'package:mongodb_flutter/pages/Test.dart';
import 'package:mongodb_flutter/pages/webview.dart';
import 'package:mongodb_flutter/pages/signup.dart';
import 'package:mongodb_flutter/pages/login.dart';
import 'package:mongodb_flutter/pages/contact_support.dart';
import 'package:mongodb_flutter/pages/delete_account.dart';
import 'package:mongodb_flutter/pages/edit_profile_page.dart';
import 'package:mongodb_flutter/pages/terms.dart';
import 'package:mongodb_flutter/pages/search_user_page.dart';
import 'package:mongodb_flutter/database/database.dart';
import 'package:mongodb_flutter/models/user.dart';
import 'package:mongodb_flutter/components/user_card.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int age;
  TextEditingController _searchController;

  void setAgeFilter(int enteredAge) {
    setState(() {
      age = enteredAge;
    });
  }

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 14, 13, 13),
        flexibleSpace: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.only(top: 60.0),
                child: Text(
                  'Search',
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              bottom: 0,
              child: Padding(
                padding: const EdgeInsets.only(top: 50.0, right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        // builder: (context) => WebViewPage(),
                        builder: (context) => SeachPage(),
                      ),
                    );
                  },
                  child: Image.asset(
                    'assets/Insuppli2.png', // Replace with the actual path to your image
                    width: 35,
                    height: 35,
                  ),
                ),
              ),
            ),
          ],
        ),
        leading: Theme(
          data: Theme.of(context).copyWith(
            popupMenuTheme: PopupMenuThemeData(
              color: Colors.black, // Set the desired background color here
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                    15), // Set the desired border radius here
              ),
            ),
          ),
          // child: Stack(
          // children: [
          // Positioned(
          // left: 30,
          // top: 0,
          // child: Positioned(
//             left: 60,
          // top: 60,
          child: PopupMenuButton<String>(
            icon: Icon(
              Icons.menu,
              color: Colors.green,
              size: 40,
            ),
            onSelected: (value) {
              if (value == 'edit_profile') {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EditProfilePage(),
                  ),
                );
              } else if (value == 'support') {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ContactFormPage(),
                  ),
                );
              } else if (value == 'terms') {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => WebViewPage(),
                  ),
                );
              } else if (value == 'delete_account') {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => DeleteAccountPopup(),
                );
              } else if (value == 'sign_out') {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text('Logout'),
                      content: const Text('Are you sure you want to log out?'),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context); // Close the dialog
                          },
                          child: const Text('Cancel'),
                        ),
                        TextButton(
                          onPressed: () async {
                            // Call the logout function
                            // await logout();
                            // Navigate to the login page
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginPage()),
                            );
                          },
                          child: const Text('Logout'),
                        ),
                      ],
                    );
                  },
                );
              }
            },
            itemBuilder: (BuildContext context) => [
              PopupMenuItem<String>(
                value: 'edit_profile',
                child: Text(
                  'Edit Profile',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold, // Add bold font
                      fontSize: 17), // Set the desired text color here
                ),
              ),
              PopupMenuItem<String>(
                value: 'support',
                child: Text(
                  'Support',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold, // Add bold font
                      fontSize: 17), // Set the desired text color here
                ),
              ),
              PopupMenuItem<String>(
                value: 'terms',
                child: Text(
                  'Terms',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold, // Add bold font
                      fontSize: 17), // Set the desired text color here
                ),
              ),
              PopupMenuItem<String>(
                value: 'delete_account',
                child: Text(
                  'Delete Account',
                  style: TextStyle(
                    color: Color.fromARGB(255, 252, 3, 3),
                    fontWeight: FontWeight.bold, // Add bold font
                    fontSize: 17,
                  ), // Set the desired text color here
                ),
              ),
              PopupMenuItem<String>(
                value: 'sign_out',
                child: Text(
                  'Sign Out',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold, // Add bold font
                    fontSize: 17,
                  ), // Set the desired text color here
                ),
              ),
            ],
          ),
          // ),
          // ),
          // ],
          // ),
        ),
      ),
      backgroundColor: Colors.black,
      body: Column(
        children: [
          Image.asset(
            'assets/insupplii.png',
            width: 200,
            height: 100,
          ),
          Align(
            alignment: Alignment.center,
            child: Text.rich(
              TextSpan(
                text: 'Search, Find, Buy\n',
                style: TextStyle(fontSize: 18, color: Colors.white),
                children: [
                  TextSpan(
                    text: 'Industrial Supplies Near You',
                  ),
                ],
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16.0, bottom: 16.0),
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: TextField(
                      controller: _searchController,
                      onChanged: (value) {
                        int enteredAge = int.tryParse(value);
                        if (enteredAge != null) {
                          setAgeFilter(enteredAge);
                        }
                      },
                      decoration: InputDecoration(
                        hintText: 'Enter your search query',
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                ),
                // ElevatedButton(
                //   onPressed: () {
                //     int enteredAge = int.tryParse(_searchController.text);
                //     if (enteredAge != null) {
                //       setAgeFilter(enteredAge);
                //     }
                //   },
                //   child: Text('Submit'),
                // ),
              ],
            ),
          ),
          Expanded(
            child: FutureBuilder(
              future: MongoDatabase.getDocumentsByAge(age),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  if (snapshot.hasError) {
                    return Center(
                      child: Text(
                        'Something went wrong, try again.',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    );
                  } else {
                    return ListView.builder(
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: UserCard(
                            user: User.fromMap(snapshot.data[index]),
                            onTapDelete: () async {
                              _deleteUser(User.fromMap(snapshot.data[index]));
                            },
                            onTapEdit: () async {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) {
                                    return AddUserPage();
                                  },
                                  settings: RouteSettings(
                                    arguments:
                                        User.fromMap(snapshot.data[index]),
                                  ),
                                ),
                              ).then((value) => setState(() {}));
                            },
                          ),
                        );
                      },
                      itemCount: snapshot.data.length,
                    );
                  }
                }
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) {
                return AddUserPage();
              },
            ),
          ).then((value) => setState(() {}));
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.green,
      ),
    );
  }

  void _deleteUser(User user) async {
    await MongoDatabase.delete(
        user); // Pass the entire user object to the delete method
    setState(() {});
  }
}
