// // ignore_for_file: use_key_in_widget_constructors, library_private_types_in_public_api, prefer_final_fields, avoid_print, prefer_const_constructors, deprecated_member_use, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class ContactFormPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: 'https://insuppli.com/contact-us',
      //
      withJavascript: true,
      withLocalStorage: true,
    );
  }
}

// import 'package:flutter/material.dart';

// class ContactFormPage extends StatefulWidget {
//   @override
//   _ContactFormPageState createState() => _ContactFormPageState();
// }

// class _ContactFormPageState extends State<ContactFormPage> {
//   TextEditingController _nameController = TextEditingController();
//   TextEditingController _emailController = TextEditingController();
//   TextEditingController _messageController = TextEditingController();

//   @override
//   void dispose() {
//     _nameController.dispose();
//     _emailController.dispose();
//     _messageController.dispose();
//     super.dispose();
//   }

//   void _saveContactChanges() {
//     String name = _nameController.text;
//     String email = _emailController.text;
//     String message = _messageController.text;

//     print('Name: $name');
//     print('Email: $email');
//     print('Message: $message');
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.black,
//       appBar: AppBar(
//         centerTitle: true,
//         backgroundColor: Colors.black,
//         leading: IconButton(
//           icon: Icon(
//             Icons.close,
//             color: Colors.green,
//           ),
//           onPressed: () {
//             Navigator.pop(context);
//           },
//         ),
//         actions: [
//           Padding(
//             padding: EdgeInsets.only(top: 15, right: 90.0),
//             child: Image.asset(
//               'assets/insupplii.png',
//               height: 350,
//               width: 200,
//             ),
//           ),
//         ],
//       ),
//       body: Column(
//         crossAxisAlignment: CrossAxisAlignment.stretch,
//         children: [
//           // Padding(
//           //   padding: const EdgeInsets.all(16.0),
//           //   child: Transform.translate(
//           //     offset: Offset(0.0, -10.0),
//           //     child: Image.asset(
//           //       'assets/insupplii.png',
//           //       height: 10,
//           //       width: 10,
//           //     ),
//           //   ),
//           // ),
//           Container(
//             margin: EdgeInsets.only(top: 50),
//             alignment: Alignment.center,
//             child: Text(
//               'N E E D   S O M E   H E L P?',
//               style: TextStyle(
//                 color: Colors.white,
//                 fontSize: 22,
//               ),
//             ),
//           ),
//           Container(
//             margin: EdgeInsets.only(top: 50),
//             alignment: Alignment.center,
//             child: Text(
//               'DROP US A LINE!',
//               style: TextStyle(
//                 color: Colors.white,
//                 fontSize: 17,
//               ),
//             ),
//           ),
//           Expanded(
//             child: Padding(
//               padding: const EdgeInsets.all(16.0),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.stretch,
//                 children: [
//                   TextField(
//                     controller: _nameController,
//                     style: TextStyle(color: Colors.white),
//                     decoration: InputDecoration(
//                       hintText: 'Name',
//                       hintStyle: TextStyle(color: Colors.white),
//                       filled: true,
//                       fillColor: Color.fromARGB(
//                           255, 20, 17, 17), // Set background color to grey
//                     ),
//                   ),
//                   SizedBox(height: 16.0),
//                   TextField(
//                     controller: _emailController,
//                     style: TextStyle(color: Colors.white),
//                     decoration: InputDecoration(
//                       hintText: 'Email',
//                       hintStyle: TextStyle(color: Colors.white),
//                       filled: true,
//                       fillColor: Color.fromARGB(
//                           255, 20, 17, 17), // Set background color to grey
//                     ),
//                   ),
//                   SizedBox(height: 16.0),
//                   TextField(
//                     controller: _messageController,
//                     style: TextStyle(color: Colors.white),
//                     decoration: InputDecoration(
//                       hintText: 'Message',
//                       hintStyle: TextStyle(color: Colors.white),
//                       filled: true,
//                       fillColor: Color.fromARGB(
//                           255, 20, 17, 17), // Set background color to grey
//                     ),
//                   ),
//                   SizedBox(height: 16.0),
//                   Text(
//                     'THIS WEBSITE USES COOKIES.',
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 18,
//                     ),
//                   ),
//                   SizedBox(height: 16.0),
//                   Text(
//                     'We use cookies to analyze website traffic and\n'
//                     'optimize your website experience. By accepting\n'
//                     'our use of cookies, your data will be aggregated\n'
//                     'with all other user data.',
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 16,
//                     ),
//                   ),
//                   Spacer(),
//                 ],
//               ),
//             ),
//           ),
//           Align(
//             alignment: Alignment.bottomCenter,
//             child: Padding(
//               padding:
//                   const EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
//               child: ElevatedButton(
//                 onPressed: _saveContactChanges,
//                 style: ElevatedButton.styleFrom(
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(20.0),
//                   ),
//                   primary: Colors.white,
//                 ),
//                 child: Container(
//                   width: double
//                       .infinity, // Make the button expand to fill the available space
//                   child: Center(
//                     child: Text(
//                       'ACCEPT',
//                       style: TextStyle(color: Colors.black),
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
