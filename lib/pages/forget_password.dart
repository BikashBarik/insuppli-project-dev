import 'package:flutter/material.dart';
import 'package:mongo_dart/mongo_dart.dart' as M;
import 'package:mongodb_flutter/database/database.dart';
import 'package:mongodb_flutter/models/adduser.dart';
import 'package:flutter/gestures.dart';
import 'package:mongodb_flutter/pages/home_page.dart';
import 'package:mongodb_flutter/pages/login.dart';

class ForgetPasswordPage extends StatelessWidget {
  const ForgetPasswordPage({Key key}) : super(key: key);

  static const String _title = 'Forget Password Page';

  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          children: [
            Container(
              padding: const EdgeInsets.fromLTRB(10, 60, 10, 0),
              color: Color.fromARGB(255, 36, 36, 36),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(Icons.close, color: Colors.green),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LoginPage(),
                        ),
                      );
                    },
                  ),
                  Text(
                    'Forgot Password',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(width: 40), // Add spacing for center alignment
                ],
              ),
            ),
            Expanded(
              child: MyStatefulWidget(),
            ),
          ],
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController companyNameController = TextEditingController();
  TextEditingController emailAddressController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    firstNameController.dispose();
    lastNameController.dispose();
    companyNameController.dispose();
    emailAddressController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final AddUser user = ModalRoute.of(context).settings.arguments;
    if (user != null) {
      firstNameController.text = user.fname;
      lastNameController.text = user.lname;
      companyNameController.text = user.cname;
      emailAddressController.text = user.email;
      passwordController.text = user.pass;
    }
    return Padding(
      padding: const EdgeInsets.all(10),
      child: ListView(
        children: <Widget>[
          Transform.translate(
            offset: const Offset(0, 20),
            child: Container(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child:
                  Image(image: AssetImage('assets/insupplii.png'), height: 80),
            ),
          ),

          // Container(
          // padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          // child: Image(image: AssetImage('assets/insupplii.png'), height: 80),
          // ),
          Transform.translate(
            offset: const Offset(0, 100),
            child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 30),
              child: const Text(
                'Enter your email below to reset your password',
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.white,
                ),
              ),
            ),
          ),

          // Container(
//             padding: const EdgeInsets.all(10),
          // margin: const EdgeInsets.only(left: 20),
          // child: TextField(
          // controller: companyNameController,
          // style: TextStyle(color: Colors.white),
          // decoration: InputDecoration(
          // border: InputBorder.none,
          // hintText: 'Company Name',
          // hintStyle: TextStyle(
          // color: Colors.white70,
          // ),
          // ),
          // ),
          // ),
          Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(left: 20, top: 100),
            child: TextField(
              controller: emailAddressController,
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Email Address',
                hintStyle: TextStyle(
                  color: Colors.white70,
                ),
              ),
            ),
          ),

          Container(
            height: 40,
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            margin: const EdgeInsets.only(left: 15, right: 15, top: 20),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(40),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(255, 124, 241, 127),
                ),
                child: const Text(
                  'Reset Password',
                  style: TextStyle(
                    color: Colors.white,
                    // color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  insertUser();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  insertUser() async {
    final adduser = AddUser(
      id: M.ObjectId(),
      fname: firstNameController.text,
      lname: lastNameController.text,
      cname: companyNameController.text,
      email: emailAddressController.text,
      pass: passwordController.text,
    );
    await MongoDatabase.insertAddUser(adduser);
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => HomePage()),
    );
  }
}
