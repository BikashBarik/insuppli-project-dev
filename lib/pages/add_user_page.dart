import 'package:flutter/material.dart';
import 'package:mongo_dart/mongo_dart.dart' as M;
import 'package:mongodb_flutter/database/database.dart';
import 'package:mongodb_flutter/models/user.dart';

class AddUserPage extends StatefulWidget {
  @override
  _AddUserPageState createState() => _AddUserPageState();
}

class _AddUserPageState extends State<AddUserPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController ageController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    phoneController.dispose();
    ageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final User user = ModalRoute.of(context).settings.arguments;
    var widgetText = 'Add Product';
    if (user != null) {
      nameController.text = user.productname;
      phoneController.text = user.price.toString();
      ageController.text = user.partno.toString();
      widgetText = 'Update Product';
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(widgetText),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: nameController,
                    decoration: InputDecoration(labelText: 'Product Name'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    keyboardType: TextInputType.phone,
                    controller: phoneController,
                    decoration: InputDecoration(labelText: 'Price'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    keyboardType: TextInputType.number,
                    controller: ageController,
                    decoration: InputDecoration(labelText: 'Part Number'),
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 4.0),
              child: ElevatedButton(
                child: Text(widgetText),
                onPressed: () {
                  if (user != null) {
                    updateUser(user);
                  } else {
                    insertUser();
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  insertUser() async {
    final user = User(
      id: M.ObjectId(),
      productname: nameController.text,
      price: int.parse(phoneController.text),
      partno: int.parse(ageController.text),
    );
    await MongoDatabase.insert(user);
    Navigator.pop(context);
  }

  updateUser(User user) async {
    print('updating: ${nameController.text}');
    final u = User(
      id: user.id,
      productname: nameController.text,
      price: int.parse(phoneController.text),
      partno: int.parse(ageController.text),
    );
    await MongoDatabase.update(u);
    Navigator.pop(context);
  }
}
