// ignore_for_file: use_key_in_widget_constructors, prefer_final_fields, unused_local_variable, prefer_const_constructors, library_private_types_in_public_api, avoid_print, deprecated_member_use, sized_box_for_whitespace

import 'package:flutter/material.dart';

class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _companyController = TextEditingController();

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _companyController.dispose();
    super.dispose();
  }

  void _saveChanges() {
    // Implement the logic to save the changes here
    String firstName = _firstNameController.text;
    String lastName = _lastNameController.text;
    String company = _companyController.text;

    // Perform any necessary actions with the updated profile data

    // Display the values in the console
    print('First Name: $firstName');
    print('Last Name: $lastName');
    print('Company: $company');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(
          'Edit Profile',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: Icon(
            Icons.close,
            color: Colors.green,
          ),
          onPressed: () {
            Navigator.pop(
                context); // Navigate back to the previous page (Home page)
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 50.0),
            child: Image.asset(
              'assets/insupplii.png',
              height: 200,
              width: 220,
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Text(
              'Edit Profile Details Below',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  TextField(
                    controller: _firstNameController,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      hintText: 'First Name',
                      hintStyle: TextStyle(color: Colors.white),
                      filled: true,
                      fillColor: Colors.black,
                    ),
                  ),
                  SizedBox(height: 16.0),
                  TextField(
                    controller: _lastNameController,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      hintText: 'Last Name',
                      hintStyle: TextStyle(color: Colors.white),
                      filled: true,
                      fillColor: Colors.black,
                    ),
                  ),
                  SizedBox(height: 16.0),
                  TextField(
                    controller: _companyController,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      hintText: 'Company',
                      hintStyle: TextStyle(color: Colors.white),
                      filled: true,
                      fillColor: Colors.black,
                    ),
                  ),
                  ElevatedButton(
                    onPressed: _saveChanges,
                    style: ElevatedButton.styleFrom(
                      padding: EdgeInsets.symmetric(
                          horizontal: 100.0,
                          vertical: 20.0), // Adjust padding value
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      primary: Color.fromARGB(255, 22, 197, 28),
                    ),
                    child: Container(
                      width: double.infinity,
                      child: Center(
                        child: Text(
                          'Save Changes',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
