import 'package:mongo_dart/mongo_dart.dart';
import 'package:mongodb_flutter/models/user.dart';
import 'package:mongodb_flutter/models/adduser.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MongoDatabase {
  static Db db;
  static DbCollection userCollection;
  static DbCollection newuserCollection;

  static Future<void> connect() async {
    db = await Db.create('mongodb://localhost:27017/Test');
    await db.open();
    userCollection = db.collection('newusers');
    newuserCollection = db.collection('adduser');
  }

  static Future<List<Map<String, dynamic>>> getDocuments() async {
    try {
      final users = await userCollection.find().toList();
      return users;
    } catch (e) {
      print(e);
      throw Exception('Failed to retrieve documents: $e');
    }
  }

  static Future<List<Map<String, dynamic>>> getDocumentsByAge(
      int partno) async {
    try {
      final users =
          await userCollection.find(where.eq('partno', partno)).toList();
      return users;
    } catch (e) {
      print(e);
      throw Exception('Failed to retrieve documents by age: $e');
    }
  }

  static Future<void> insert(User user) async {
    await userCollection.insertAll([user.toMap()]);
  }

  static Future<void> update(User user) async {
    final u = await userCollection.findOne({"_id": user.id});
    u["productname"] = user.productname;
    u["partno"] = user.partno;
    u["price"] = user.price;
    await userCollection.save(u);
  }

  static Future<void> delete(User user) async {
    await userCollection.remove(where.id(user.id));
  }

  static Future<void> insertAddUser(AddUser adduser) async {
    await newuserCollection.insertAll([adduser.toMap()]);
  }

  static Future<AddUser> login(String email, String password) async {
    try {
      final user = await newuserCollection
          .findOne(where.eq('email', email).eq('pass', password));

      if (user != null) {
        return AddUser.fromMap(user);
      } else {
        return null; // User not found or invalid credentials
      }
    } catch (e) {
      print(e);
      throw Exception('Failed to perform login: $e');
    }
  }

  static Future<void> logout() async {
    // Clear session data
    clearSessionData();
    // Clear user credentials
    clearUserCredentials();
    // Sign out from the authentication service
    signOutFromAuthService();
  }

  static Future<void> clearSessionData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  static Future<void> clearUserCredentials() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('eamil');
    await prefs.remove('password');
  }

  static void signOutFromAuthService() {
    // Implement the logic to sign out from the authentication service
    // This will depend on the authentication service you are using
    // Consult the documentation or resources for your chosen authentication service
    // For example, if using Firebase Authentication:
    // FirebaseAuth auth = FirebaseAuth.instance;
    // await auth.signOut();
  }
}
