import 'package:flutter/material.dart';
import 'package:mongodb_flutter/database/database.dart';
import 'package:mongodb_flutter/pages/home_page.dart';
import 'package:mongodb_flutter/pages/login.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await MongoDatabase.connect();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext buildContext) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      // home: HomePage(),
      home: LoginPage(),
      // ),
    );
  }
}
